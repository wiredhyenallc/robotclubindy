import pygame
from math import *
from random import randint
from geometry import point
import csv

def clamp(num, min_value, max_value):
   return max(min(num, max_value), min_value)

#initiate pygame
pygame.init()

#create a 600x600 px drawingwindow
screen = pygame.display.set_mode((601, 601))

#flag set to true on window exit
done = False


#class used to maintain 30fps
clock = pygame.time.Clock()


#declare the speed at which turning occurs without throttle
lateralSlipImpulse=15

#determine friction impulse
frictionImpulse=2

#for now, start with 0 throttle
throttle=0

#number of pixels per unit length in the simulation
gridSize=18

#maximum number of units towards the east direction in the world
maxUnits =floor(300/gridSize)

#amount the heading can turn per frame, in terms of desired heading
headingChangeSpeed=0.75

track=[]
with open('track.tsv', 'r') as file:
    reader = csv.reader(file,delimiter="\t")
    for row in reader:
        track.append(point(float(row[0]),float(row[1])))



#function to convert world dimension to screen pixels (e.g. units to pixels)
def dimension(worldDimension):
    return floor(gridSize*worldDimension)


#track the car's position
car=track[0].copy()

#track the car's velocity
velocity=track[1].sub(track[0]).normalized()

heading=0

wayPoint=track[1].copy()


#handle arrow key presses
arrowKeys={"up":False,"down":False,"left":False,"right":"False"}
def isKeyDown(key):
    return arrowKeys[key]

#determine if position is within the screen, with padding (in world units)
def inScreen(pt,padding=1):
    
    if gridSize*pt.x> -300+padding*gridSize and gridSize*pt.x< 300-padding*gridSize:
        if gridSize*pt.y> -300+padding*gridSize and gridSize*pt.y< 300-padding*gridSize:
            return True
    return False



#steering algorithm
def steer():

    #make global steering variables writeable
    global heading 
    global throttle

    #get the velocity relative coordinates of the waypoint
    vecCarWaypoint=wayPoint.sub(car)

    #forward and lateral components of the relative position of the waypoint with respect to car position and velocity
    forwardVector=velocity.normalized()
    leftVector=point(-velocity.y,velocity.x).normalized()
    wpRelative=point(-vecCarWaypoint.scalarProjOnto(leftVector),vecCarWaypoint.scalarProjOnto(forwardVector))

    
    
    

    #set the throttle to maintain around 10 units/sec
    veloc=velocity.magnitude()

  

 
    throttle=vecCarWaypoint.magnitude()/2.5-4.5

    
    if veloc>10:
        throttle=4
    
    if veloc<6:
        throttle=6

    #steer in the lateral direction matching whether the the waypoint is left or right of the car
    heading=0
    desiredHeading=vecCarWaypoint.angleTo(forwardVector)
    if wpRelative.x>0:
        desiredHeading*=-1
    heading=desiredHeading
    heading=clamp(heading,-pi/2.1,pi/2.1)
    





    


#main game loop
while not done:
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True               
        pressed = pygame.key.get_pressed()
        arrowKeys["up"]=False
        arrowKeys["down"]=False
        arrowKeys["left"]=False
        arrowKeys["right"]=False
        if pressed[pygame.K_UP]: arrowKeys["up"]=True
        if pressed[pygame.K_DOWN]: arrowKeys["down"]=True
        if pressed[pygame.K_LEFT]: arrowKeys["left"]=True
        if pressed[pygame.K_RIGHT]: arrowKeys["right"]=True
        
        #clear screen to white
        screen.fill((255, 255, 255))

        #draw the grid
        for i in range(maxUnits):
            linePoints=[point(i,maxUnits),point(i,-maxUnits),point(-i,maxUnits),point(-i,-maxUnits),point(maxUnits,i),point(-maxUnits,i),point(maxUnits,-i),point(-maxUnits,-i)]
            for j in range(floor(len(linePoints)/2)):
                pygame.draw.line(screen,(0,0,0),(linePoints[j*2].shiftedX(),linePoints[j*2].shiftedY()),(linePoints[j*2+1].shiftedX(),linePoints[j*2+1].shiftedY()),floor(gridSize/8))
            

        #paint the car's position
        numPixels = dimension(0.75)
        pygame.draw.ellipse(screen,(255,0,0),pygame.Rect(car.shiftedX()-numPixels/2,car.shiftedY()-numPixels/2,numPixels,numPixels))
     
        #calculate new car position based on velocity
        newCarPositionX=car.x+velocity.x/30
        newCarPositionY=car.y+velocity.y/30
        newCarVLinePosition=point(car.x+10*velocity.x/30,car.y+10*velocity.y/30)
        

        #draw a velocity line
        pygame.draw.line(screen,(0,255,0),(car.shiftedX(),car.shiftedY()),(newCarVLinePosition.shiftedX(),newCarVLinePosition.shiftedY()),floor(gridSize/4))


        #track the car's heading
        velAngle=velocity.angleOf()

        #get the heading vector
        headingVector=point(cos(velAngle+heading),sin(velAngle+heading))

        #get the heading vectors tangential and orthagonal components to the velocity
        headingTangential=headingVector.projOnto(velocity)
        headingNormal=headingVector.sub(headingTangential)
    
        #draw a h line
        newCarHLinePosition=point(car.x+50*headingVector.x/30,car.y+50*headingVector.y/30)
        pygame.draw.line(screen,(0,0,255),(car.shiftedX(),car.shiftedY()),(newCarHLinePosition.shiftedX(),newCarHLinePosition.shiftedY()),floor(gridSize/4))

        #update car's position according to velocity for the next frame, 30 frames per second so 1/30 seconds per frame
        if inScreen(car):
            car.x=newCarPositionX
            car.y=newCarPositionY
        else:
            car=point(randint(-maxUnits+1,maxUnits-1),randint(-maxUnits+1,maxUnits-1))
        #update the car's velocity according to the heading
        velocReverse=velocity.normalized().scaled(-1)
        newVelocityX=velocity.x+velocReverse.x*frictionImpulse/30+throttle*headingVector.x/30+lateralSlipImpulse*headingNormal.x/30
        newVelocityY=velocity.y+velocReverse.y*frictionImpulse/30+throttle*headingVector.y/30+lateralSlipImpulse*headingNormal.y/30
        newVelocity=point(newVelocityX,newVelocityY)
        if newVelocity.magnitude()<=2:
            newVelocity=velocity
        velocity=newVelocity
        
        throttle=0
        heading=0
      
        #if there is a waypoint, steer
        if wayPoint != None:

            #draw the waypoint
            pygame.draw.ellipse(screen,(0,255,0),pygame.Rect(wayPoint.shiftedX()-gridSize/2,wayPoint.shiftedY()-gridSize/2,gridSize,gridSize))
            
            
            #draw the track
            for index in range(len(track)-1):
                p1=track[index]
                p2=track[index+1]
                pygame.draw.line(screen,(255,0,0),(p1.shiftedX(),p1.shiftedY()),(p2.shiftedX(),p2.shiftedY()),floor(gridSize/9))
            #run the steering algorithm, this adjsuts global heading and throttle variables
            steer()
            wayPoint=track[-1].copy()
            
        pygame.display.flip()
        clock.tick(30)