#trackgen.py
#functions to build tracks using lines and circles
#cx, cy will be input into the functions, and appended to in place

import math

def gen_arc(cx,cy,centerX, centerY, radius, lowAngle, highAngle, resolution):
    for i in range(resolution):
        angle=lowAngle+(highAngle-lowAngle)*i/(resolution)
        cx.append(centerX+radius*math.cos(angle))
        cy.append(centerY+radius*math.sin(angle))

def gen_line(cx,cy,p1x,p1y,p2x,p2y,resolution):
    for i in range(resolution):
        cx.append(p1x+(p2x-p1x)*(i/resolution))
        cy.append(p1y+(p2y-p1y)*(i/resolution))