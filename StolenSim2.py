"""

Path tracking simulation with pure pursuit steering and PID speed control.

author: Atsushi Sakai (@Atsushi_twi)
        Guillaume Jacquenot (@Gjacquenot)

"""
import numpy as np
import math
import matplotlib.pyplot as plt
import keyboard
from geometry import point
from random import randint
from trackgen import *

# Parameters
k = 0.1  # look forward gain
Lfc = 2.0  # [m] look-ahead distance
Kp = 1.0  # speed proportional gain
dt = 0.1  # [s] time tick
WB = 2.9  # [m] wheel base of vehicle

show_animation = True
cx=[]
cy=[]

long_straightaway=200
short_straightaway=40
corner_radius=2/math.pi*100

gen_line(cx,cy,0,0,long_straightaway,0,5)
gen_arc(cx,cy,long_straightaway,-corner_radius,corner_radius,math.pi/2,0,5)


gen_line(cx,cy,long_straightaway+corner_radius,-corner_radius,long_straightaway+corner_radius,-corner_radius-short_straightaway,3)
gen_arc(cx,cy,long_straightaway,-corner_radius-short_straightaway,corner_radius,0,-math.pi/2,5)



gen_line(cx,cy,long_straightaway,-corner_radius-short_straightaway-corner_radius,0,-corner_radius-short_straightaway-corner_radius,5)
gen_arc(cx,cy,0,-corner_radius-short_straightaway,corner_radius,-math.pi/2,-math.pi,5)



gen_line(cx,cy,-corner_radius,-corner_radius-short_straightaway,-corner_radius,-corner_radius,3)
gen_arc(cx,cy,0,-corner_radius,corner_radius,-math.pi,-3*math.pi/2,5)
waypointIndex=0
waypoint=point(cx[0],cy[0])
 
def clamp(num,low,high):
    if num<low:
        num=low
    if num>high:
        num=high
    return num

class State:

    def __init__(self, x=0.0, y=0.0, yaw=0.0, v=0.0):
        self.index=0
        self.x = x
        self.y = y
        self.yaw = yaw
        self.v = v
        self.rear_x = self.x - ((WB / 2) * math.cos(self.yaw))
        self.rear_y = self.y - ((WB / 2) * math.sin(self.yaw))

    def update(self, a, delta):
        self.x += self.v * math.cos(self.yaw) * dt
        self.y += self.v * math.sin(self.yaw) * dt
        self.yaw += self.v / WB * math.tan(delta) * dt
        self.v += a * dt
        self.rear_x = self.x - ((WB / 2) * math.cos(self.yaw))
        self.rear_y = self.y - ((WB / 2) * math.sin(self.yaw))
        self.index+=1

    def calc_distance(self, point_x, point_y):
        dx = self.rear_x - point_x
        dy = self.rear_y - point_y
        return math.hypot(dx, dy)


class States:

    def __init__(self):
        self.x = []
        self.y = []
        self.yaw = []
        self.v = []
        self.t = []

    def append(self, t, state):
        self.x.append(state.x)
        self.y.append(state.y)
        self.yaw.append(state.yaw)
        self.v.append(state.v)
        self.t.append(t)
    

def plot_arrow(x, y, yaw, length=4.0, width=2.0, fc="r", ec="k"):
    """
    Plot arrow
    """

    if not isinstance(x, float):
        for ix, iy, iyaw in zip(x, y, yaw):
            plot_arrow(ix, iy, iyaw)
    else:
        plt.arrow(x, y, length * math.cos(yaw), length * math.sin(yaw),
                  fc=fc, ec=ec, head_width=width, head_length=width)
        plt.plot(x, y)

def getNextWaypoint(advance=1):
    global cx,cy,waypoint,waypointIndex
    nextIndex=waypointIndex+advance
    while nextIndex>=len(cx):
        nextIndex-=len(cx)
    while nextIndex<0:
        nextIndex+=len(cx)
    return point(cx[nextIndex],cy[nextIndex])


    

def get_la_point(state,distanceResolution=0.05,look_ahead_distance=15):


    car=point(state.x,state.y)
    veloc=point(math.cos(state.yaw)*state.v,math.sin(state.yaw)*state.v)    
    closest_point=waypoint    
    closest_point_distance=10000000

    for i in range (-2,3):
        try:
            this=getNextWaypoint(i)
            next=getNextWaypoint(i+1)
            vec1=next.sub(this)
            vec1n=vec1.normalized()
            numSegments=math.floor(vec1.magnitude()/distanceResolution)
            for j in range(numSegments):
                searchPoint=this.sum(vec1n.scaled(distanceResolution*j))
                dist=searchPoint.sub(car).magnitude()
                if dist<closest_point_distance and dist>look_ahead_distance and searchPoint.sub(car).scalarProjOnto(veloc)>0.005 :
                    closest_point_distance=dist
                    closest_point=searchPoint

        except:
            pass
    return closest_point   

def steer(state):
    la_point=get_la_point(state)

    car=point(state.x,state.y)   
    velvec=point(math.cos(state.yaw),math.sin(state.yaw))
    rightvec=velvec.getLeft().scaled(-1)
    veccarwaypoint=la_point.sub(car)
    top_speed=40
    target_veloc=top_speed
    if (state.v>target_veloc):
        accell=-target_veloc/8
    else:
        accell=target_veloc/10
   
    steeringAngle=veccarwaypoint.angleTo(velvec)
    if veccarwaypoint.scalarProjOnto(rightvec) > 0:
        steeringAngle*=-1   
    
    
    steeringAngle=clamp(steeringAngle,-math.pi/4,math.pi/4)
    return accell, steeringAngle
def main():


    global waypoint,waypointIndex
    state = State(x=-0.0, y=-3.0, yaw=0.0, v=0.0)
    time = 0.0
    states = States()
    states.append(time, state)
   
    
    while not keyboard.is_pressed('q'):
        
        # Calc control input
        ai,di=steer(state)

        state.update(ai, di)  # Control vehicle
        if waypoint.sub(point(state.x,state.y)).magnitude()<4:
            waypointIndex+=1
            try:
                waypoint=point(cx[waypointIndex],cy[waypointIndex])
            except:
                waypointIndex=0
                waypoint=point(cx[waypointIndex],cy[waypointIndex])
        time += dt
        states.append(time, state)

        if show_animation:  # pragma: no cover
            plt.cla()
           
            plot_arrow(state.x, state.y, state.yaw)
            plt.plot(states.x, states.y, "-b", label="trajectory")
            plt.plot(waypoint.x,waypoint.y,"*r",label="waypoint")
            plt.plot(cx,cy,":k",label="course")
            la_point=get_la_point(state)
        
            plt.plot(la_point.x,la_point.y,"*g")

            #plt.axis("equal")
            plt.gca().set_aspect('equal', adjustable='box')
            plt.grid(True)
            plt.title("Speed[m/s]:" + str(state.v)[:4])
            plt.pause(0.001)

  

if __name__ == '__main__':
    print("Pure pursuit path tracking simulation start")
    main()
