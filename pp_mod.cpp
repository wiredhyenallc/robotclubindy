#include <ros/ros.h>
#include "pure_pursuit.hpp"
#include <sstream>
#include "visualization_msgs/MarkerArray.h"
#define _USE_MATH_DEFINES
#include <cmath>
#define desired_speed 10.0
#define acceleration_scale 0.1


namespace ns_pure_pursuit {

//convert any angle to the range 0 to 2*M_PI
float principleAngle(float angle){
	while (angle < 0)
		angle+=2*M_PI;
	while (angle >=2*M_PI)
		angle-=2*M_PI;
	return angle;
}

//find the desired steering angle between two headings
float smallAngle(float angle1, float angle2){
	float try1=principleAngle(angle1)-principleAngle(angle2);
	
	if (try1 >0){
		if( std::abs(try1) >M_PI)
			try1-=2*M_PI;
	}else{
		if(std::abs(try1)>M_PI)
			try1+=2*M_PI;
	}
	return try1;
}

//clamp a float
float clip(float n, float lower, float upper) {
  return std::max(lower, std::min(n, upper));
}


// Constructor
PurePursuit::PurePursuit(ros::NodeHandle &nh) : nh_(nh) {
    pub_closest_point_ = nh.advertise<visualization_msgs::MarkerArray>("/control/pure_pursuit/marker", 1);

    if (!nh.param<double>("controller/speed/p", speed_p, 0.01)) {
        ROS_WARN_STREAM("Did not load controller/speed/p. Standard value is: " << 0.01);
    }
    if (!nh.param<double>("controller/steering/p", steering_p, 0.01)) {
        ROS_WARN_STREAM("Did not load controller/steering/p. Standard value is: " << 0.01);
    }
};

// Getters
fsd_common_msgs::ControlCommand PurePursuit::getControlCommand() const { return control_command_; }

// Setters
void PurePursuit::setMaxSpeed(double &max_speed) {
    max_speed_ = max_speed;
}

void PurePursuit::setCenterLine(const geometry_msgs::Polygon &center_line) {
    center_line_ = center_line;
}

void PurePursuit::setState(const fsd_common_msgs::CarState &state) {
    state_ = state;
}

void PurePursuit::setVelocity(const fsd_common_msgs::CarStateDt &velocity) {
    velocity_ = velocity;
}

void PurePursuit::runAlgorithm() {
    createControlCommand();
}

void PurePursuit::createControlCommand() {

    if (center_line_.points.empty()) {
        control_command_.throttle.data       = static_cast<float>(-1.0);
        control_command_.steering_angle.data = 0.0;
        return;
    }

	//car velocity
   	const double vel = std::hypot(state_.car_state_dt.car_state_dt.x, state_.car_state_dt.car_state_dt.y);

	//closest point to car
    const auto             it_center_line = std::min_element(center_line_.points.begin(), center_line_.points.end(),
                                                             [&](const geometry_msgs::Point32 &a,
                                                                 const geometry_msgs::Point32 &b) {
                                                                 const double da = std::hypot(state_.car_state.x - a.x,
                                                                                              state_.car_state.y - a.y);
                                                                 const double db = std::hypot(state_.car_state.x - b.x,
                                                                                              state_.car_state.y - b.y);

                                                                 return da < db;
                                                             });


    const auto             i_center_line  = std::distance(center_line_.points.begin(), it_center_line);
    const auto             size           = center_line_.points.size();
	
	geometry_msgs::Point32  lookaheadVector;


	lookaheadVector.x=-state_.car_state.x;
	lookaheadVector.y=-state_.car_state.y;




		geometry_msgs::Point32 next_point = center_line_.points[(i_center_line + 40) % size];
		lookaheadVector.x+=0.6*next_point.x;
		lookaheadVector.y+=0.6*next_point.y;

		next_point = center_line_.points[(i_center_line + 80) % size];
		lookaheadVector.x+=0.4*next_point.x;
		lookaheadVector.y+=0.4*next_point.y;



	float carAngle=state_.car_state.theta;
	float lookaheadAngle=std::atan2(lookaheadVector.y,lookaheadVector.x);
	float sa =smallAngle(lookaheadAngle,state_.car_state.theta);
	float saa=std::abs(sa);
	control_command_.steering_angle.data=control_command_.steering_angle.data+(sa-control_command_.steering_angle.data)*std::exp(2.0*saa/M_PI-1.0);
	float dsc = clip(1.0-std::exp(saa*2.0/M_PI-1.0),0.4,1.0);
	
	




        control_command_.throttle.data = static_cast<float>(acceleration_scale * (dsc*desired_speed - vel));
    


}


}