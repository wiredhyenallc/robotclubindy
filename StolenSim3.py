"""

Path tracking simulation with pure pursuit steering and PID speed control.

author: Atsushi Sakai (@Atsushi_twi)
        Guillaume Jacquenot (@Gjacquenot)

"""
import numpy as np
import math
import matplotlib.pyplot as plt
import keyboard
from geometry import point
from random import randint
from gridtotrack import *


# Parameters
k = 0.1  # look forward gain
Lfc = 2.0  # [m] look-ahead distance
Kp = 1.0  # speed proportional gain
dt = 0.1  # [s] time tick
WB = 2.9  # [m] wheel base of vehicle

show_animation = True
cx=[]
cy=[]


buildTrack(cx,cy)





waypointIndex=0
waypoint=point(cx[0],cy[0])
 
def clamp(num,low,high):
    if num<low:
        num=low
    if num>high:
        num=high
    return num

class State:

    def __init__(self, x=0.0, y=0.0, yaw=0.0, v=0.0):
        self.index=0
        self.x = x
        self.y = y
        self.yaw = yaw
        self.v = v
        self.rear_x = self.x - ((WB / 2) * math.cos(self.yaw))
        self.rear_y = self.y - ((WB / 2) * math.sin(self.yaw))

    def update(self, a, delta):
        self.x += self.v * math.cos(self.yaw) * dt
        self.y += self.v * math.sin(self.yaw) * dt
        self.yaw += self.v / WB * math.tan(delta) * dt
        self.v += a * dt
        self.rear_x = self.x - ((WB / 2) * math.cos(self.yaw))
        self.rear_y = self.y - ((WB / 2) * math.sin(self.yaw))
        self.index+=1

    def calc_distance(self, point_x, point_y):
        dx = self.rear_x - point_x
        dy = self.rear_y - point_y
        return math.hypot(dx, dy)


class States:

    def __init__(self):
        self.x = []
        self.y = []
        self.yaw = []
        self.v = []
        self.t = []

    def append(self, t, state):
        self.x.append(state.x)
        self.y.append(state.y)
        self.yaw.append(state.yaw)
        self.v.append(state.v)
        self.t.append(t)
    

def plot_arrow(x, y, yaw, length=4.0, width=2.0, fc="r", ec="k"):
    """
    Plot arrow
    """

    if not isinstance(x, float):
        for ix, iy, iyaw in zip(x, y, yaw):
            plot_arrow(ix, iy, iyaw)
    else:
        plt.arrow(x, y, length * math.cos(yaw), length * math.sin(yaw),
                  fc=fc, ec=ec, head_width=width, head_length=width)
        plt.plot(x, y)


    

def get_la_point(state,distanceResolution=0.05,look_ahead_distance=10):


    car=point(state.x,state.y)
    veloc=point(math.cos(state.yaw)*state.v,math.sin(state.yaw)*state.v)    
    closest_point=None
    closest_point_distance=10000000

    for i in range(len(cx)-1):
        try:
            this=point(cx[i],cy[i])
            next=point(cx[i+1],cy[i+1])
            vec1=next.sub(this)
            vec1n=vec1.normalized()
            numSegments=math.floor(vec1.magnitude()/distanceResolution)
            for j in range(numSegments):
                searchPoint=this.sum(vec1n.scaled(distanceResolution*j))
                dist=searchPoint.sub(car).magnitude()
                if dist<closest_point_distance and dist>look_ahead_distance and searchPoint.sub(car).scalarProjOnto(veloc)>0.005 :
                    closest_point_distance=dist
                    closest_point=searchPoint

        except:
            pass
    return closest_point   

def steer(state):
    car=point(state.x,state.y)
    la_point=get_la_point(state)
    if la_point!=None: 
        veccarwaypoint=la_point.sub(car)
       
    velvec=point(math.cos(state.yaw),math.sin(state.yaw))
    rightvec=velvec.getLeft().scaled(-1)
   
    top_speed=25
    target_veloc=top_speed
    if (state.v>target_veloc):
        accell=-target_veloc/8
    else:
        accell=target_veloc/10
    steeringAngle=0
    if la_point!=None:
        steeringAngle=veccarwaypoint.angleTo(velvec)
        if veccarwaypoint.scalarProjOnto(rightvec) > 0:
            steeringAngle*=-1   
    
    
        steeringAngle=clamp(steeringAngle,-math.pi/4,math.pi/4)
    return accell, steeringAngle
def main():


    global waypoint,waypointIndex
    state = State(x=cx[0], y=cy[0], yaw=math.atan2(cy[1]-cy[0],cx[1]-cx[0]), v=0.0)
    time = 0.0
    states = States()
    states.append(time, state)
   
    
    while not keyboard.is_pressed('q'):
        
        # Calc control input
        ai,di=steer(state)

        state.update(ai, di)  # Control vehicle
        time += dt
        states.append(time, state)

        if show_animation:  # pragma: no cover
            plt.cla()
           
            plot_arrow(state.x, state.y, state.yaw)
            plt.plot(states.x, states.y, "-b", label="trajectory")
            plt.plot(cx,cy,":k",label="course")
            plt.plot([cx[-1],cx[0]],[cy[-1],cy[0]],":k",label="course")
            la_point=get_la_point(state)
            if la_point!=None:
                plt.plot(la_point.x,la_point.y,"*g")

            #plt.axis("equal")
            plt.gca().set_aspect('equal', adjustable='box')
            plt.grid(True)
            plt.title("Speed[m/s]:" + str(state.v)[:4])
            plt.pause(0.001)

  

if __name__ == '__main__':
    print("Pure pursuit path tracking simulation start")
    main()
