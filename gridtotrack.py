import csv
# reading csv file 
clockwise=[(-1,-1),(0,-1),(1,-1),(1,0),(1,1),(0,1),(-1,1),(-1,0)]
numRows=0
numCols=0
grid=[]
with open('track.csv', 'r') as csvfile: 
    csvreader = csv.reader(csvfile)  
    for i,row in enumerate(csvreader):
        grid.append([])
        numRows+=1
        for j,value in enumerate(row):
            grid[i].append(int(value))
            numCols+=1
       
maxNum=-1

for i in range(len(grid)):
    for j in range(len(grid[0])):
        if grid[i][j] >maxNum:
            maxNum=grid[i][j]
print(maxNum)   
def findInGrid(value):
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if grid[i][j]==value:
                return (i,j)
    return None
#0 = not part of track, 1 =1st segment of track
def buildTrack(cx,cy):
    for index in range(maxNum):
        position=findInGrid(index+1)
        if position !=None:
            cx.append(position[1]*10)
            cy.append(-position[0]*10)